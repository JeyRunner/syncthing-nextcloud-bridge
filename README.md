[![pipeline status](https://gitlab.com/JeyRunner/syncthing-nextcloud-bridge/badges/main/pipeline.svg)](https://gitlab.com/JeyRunner/syncthing-nextcloud-bridge/-/commits/main)
## syncthing-nextcloud-bridge
This is intended to be used in a scenario where syncthing syncs its files to a folder on you nextcloud server (e.g. at `<NEXTCLOUD-FILES-DIR>/<USER>/files/someDirToSync`).
When syncthing changed the contents of a synced folder, this program will call `occ files:scan`, so nextcloud will also notice the changes.
This avoids unnecessarily running `occ files:scan` periodically.

Note that this currently only supports notifying nextcloud about changes by syncthing, not the other way around


### Install
Download the current `.deb` package [here](https://gitlab.com/JeyRunner/syncthing-nextcloud-bridge/-/pipelines).
And install it:
```bash
dpkg -i syncthing-nextcloud-bridge_0.1.0_amd64.deb
```

### Run standalone
```bash
syncthing-nextcloud-bridge -c conf.yaml

# adapt the generated default config file
# -> insert the syncthing url and api key (get from syncthing web gui)
nano config.yaml

# restart
syncthing-nextcloud-bridge -c conf.yaml
```

### Run as systemd service
```bash
# start the systemd service
systemctl start syncthing-nextcloud-bridge.service

# adapt the generated default config file
# -> insert the syncthing url and api key (get from syncthing web gui)
nano /etc/opt/syncthing-nextcloud-bridge/config.yaml

# restart
systemctl restart syncthing-nextcloud-bridge.service

# view logs
journalctl -u syncthing-nextcloud-bridge.service
```
