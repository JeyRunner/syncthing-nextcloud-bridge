mod syncthing_api;

use clap::Parser;
use serde::{Serialize, Deserialize};
use std::{path, fs};
use std::path::Path;
use anyhow::{Result, Context};
use reqwest;
use reqwest::header;
use std::process::{Command, Stdio};
use run_script::IoOptions;
use run_script::types::ScriptOptions;
use crate::syncthing_api::SyncthingApi;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// The path to the file to read
    #[arg(short, long)]
    config_file: path::PathBuf,

    /// overwrites given config file with default values
    #[arg(long, action = clap::ArgAction::SetTrue)]
    config_file_overwrite_with_default: bool,

    /// print rest api calls to syncthing
    #[arg(short, long, action = clap::ArgAction::SetTrue)]
    verbose: bool,
}



#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ConfigYaml {
    syncthing: ConfigYamlSyncthing,

    /// Shell command to run when syncthing finished syncing a folder.
    /// This command should start nextcloud scanning.
    /// Use '%changed_dir' in the command to insert the path of the change dir.
    on_files_synced_command: String,

}
#[derive(Serialize, Deserialize, PartialEq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ConfigYamlSyncthing {
    api_url: String,
    api_key: String,
    
    /// ignore all syncthing events that where emitted before start of this program
    ignore_events_before_startup: bool,
}


fn overwrite_config_file_with_defaults(cli: &Cli) -> Result<()> {
    let defaultConfig = ConfigYaml{
        syncthing: ConfigYamlSyncthing{
            api_url: "http://localhost:8384/".into(),
            api_key: "".into(),
            ignore_events_before_startup: true,
        },
        on_files_synced_command: "echo '>>>> changed dir: %changed_dir && occ files:scan --path=%changed_dir\n'".into()
    };
    fs::create_dir_all(cli.config_file.parent().context("config-file path not valid")?)?;

    fs::write(&cli.config_file, serde_yaml::to_string(&defaultConfig).unwrap())
        .with_context(|| format!("could not write default config file to '{}'", cli.config_file.to_str().unwrap()))?;
    Ok(())
}

/// reads config file or creates default one if the file does not exist
fn read_or_default_config_file(cli: &Cli) -> Result<ConfigYaml> {
    if !cli.config_file.exists() {
        println!("Given config file does not exists, will create a new one with that name ('{}').", cli.config_file.to_str().unwrap());
        overwrite_config_file_with_defaults(&cli)?;
    }
    if cli.config_file_overwrite_with_default {
        println!("Will overwrite given config file with default values");
        overwrite_config_file_with_defaults(&cli)?;
    }

    let config_yaml: ConfigYaml = serde_yaml::from_str(fs::read_to_string(&cli.config_file)?.as_str())
        .with_context(|| "Could not read from config yaml file. \
                           \n-- To overwrite the config file with the default valid values use '--config-file-overwrite-with-default'")?;
    //println!("use config: {:#?}", config_yaml);
    Ok(config_yaml)
}


#[tokio::main]
async fn main() -> Result<()> {
    let cli = Cli::parse();
    let config_yaml = read_or_default_config_file(&cli)?;
    let on_files_synced_command = &config_yaml.on_files_synced_command;


    let on_dir_synced = move |path: &Path| {
        let on_files_synced_command_replaced = on_files_synced_command.replace("%changed_dir", path.to_str().unwrap());
        let args = vec![];
        let mut options = ScriptOptions::new();
        options.print_commands = true;
        options.output_redirection = IoOptions::Inherit;
        let exec_result = run_script::spawn(on_files_synced_command_replaced.as_str(), &args, &options)
            /*.stderr(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()*/
            .expect("on_files_synced_command failed to start")
            .wait_with_output();

    };

    let mut syncthing_api:SyncthingApi = SyncthingApi::new(
        &config_yaml,
        cli.verbose,
        &on_dir_synced
    )?;
    syncthing_api.test_connection();

    // handle sync events
    syncthing_api.event_loop()?;

    Ok(())
}
