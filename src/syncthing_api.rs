use std::alloc::System;
use std::collections::{HashMap, HashSet};
use std::fmt::Error;
use std::io::Stderr;
use std::iter::Map;
use std::path::Path;
use std::time::{Duration, Instant, SystemTime};
use chrono::{DateTime, FixedOffset, Local, NaiveDate, NaiveDateTime, NaiveTime, ParseResult, Utc};
use crate::ConfigYaml;
use anyhow::{Result, Context, bail};
use reqwest::header;
use serde_json::Value;
use async_std::task::block_on;
use clap::builder::Str;
use run_script::types::IoOptions::Inherit;
use semver::Version;
use serde::Serialize;


pub struct SyncthingApi<'a> {
    pub config_yaml: &'a ConfigYaml,
    pub client: reqwest::Client,
    verbose: bool,

    syncthing_url: reqwest::Url,
    syncthing_config_sub_url: String,
    last_event_id: i64,
    syncthing_folders: HashMap<String, Folder>,

    on_dir_synced: & 'a dyn Fn(&Path),
    on_dir_state_changed: Option<Box<dyn Fn(&Path)>>,

    startup_time: DateTime<FixedOffset>,
}

#[derive(Debug)]
struct Folder {
    path: String,
}


impl<'a> SyncthingApi<'a> {
    pub fn new(config_yaml: &'a ConfigYaml, verbose: bool, on_dir_synced: & 'a impl Fn(&Path)) -> Result<SyncthingApi<'a>> {
        let mut headers = header::HeaderMap::new();
        headers.insert("X-API-Key", header::HeaderValue::from_str(config_yaml.syncthing.api_key.as_str())?);
        let client = reqwest::Client::builder()
            .default_headers(headers)
            .build()?;

        let api = SyncthingApi{
            config_yaml,
            client,
            verbose,
            syncthing_url: reqwest::Url::parse(config_yaml.syncthing.api_url.as_str())?,
            syncthing_config_sub_url: "/rest/config".into(),
            last_event_id: -1,
            syncthing_folders: HashMap::new(),
            on_dir_synced,
            on_dir_state_changed: None,
            startup_time: DateTime::from(DateTime::<Utc>::from_utc(Local::now().naive_utc(), Utc)),
        };
        println!("will use syncthing api at '{}'", api.syncthing_url.to_string());
        return Ok(api)
    }

    /*
    pub fn set_on_dir_synced(&mut self, callback: & 'a impl Fn(&Path)) {
        self.on_dir_synced = Some(&callback);
    }
     */


    async fn make_request<T: Serialize + ?Sized>(&self, sub_url: &str, query: Option<&T>) -> Result<Value> {
        let url = self.syncthing_url.join(sub_url)?;
        if self.verbose {
            println!("get from: {}", url.to_string());
        }
        let mut request = self.client.get(url.clone());
        if let Some(query_val) = query {
            request = request.query(query_val);
        }
        let result = request.send().await?;
        if !result.status().is_success() {
            println!("-- request to '{}' returned error: {}, body: {}", url, result.status(), result.text().await.unwrap_or("".to_string()));
            return bail!("request failed");
        }

        if self.verbose {
            println!("-- result status: {}", result.status().as_str());
        }
        //println!("result: {}", result.text().await?);

        let result_json = result.json::<serde_json::Value>().await?;
        //println!("{:#?}", result_json);
        return Ok(result_json)
    }

    fn make_request_blocking_q<T: Serialize + ?Sized>(&self, sub_url: &str, query: &T) -> Result<Value> {
        return block_on(self.make_request(sub_url, Some(query)));
    }

    fn make_request_blocking(&self, sub_url: &str) -> Result<Value> {
        return block_on(self.make_request::<Vec<String>>(sub_url, None));
    }

    pub fn test_connection(&mut self) -> Result<()> {
        let result = self.make_request_blocking("/rest/system/version")?;
        let version = result["version"].as_str().unwrap();//.unwrap_or_else("??");
        let v = Version::parse(&version[1..]); // remove the v
        match v {
            Ok(v) => {
                if v <= Version::new(1, 12, 0) {
                    self.syncthing_config_sub_url = "/rest/system/config".into();
                }
            }
            Err(e) => {
                println!("could not correctly parse version string: {}", e)
            }
        }
        println!("Can connect to syncthing (version: {}).", version);
        Ok(())
    }


    pub fn event_loop(&mut self) -> Result<()> {
        loop {
            // pull new folder list
            if let Err(e) = self.pull_folders_from_config() {
                println!("could get syncthing config: {}", e);
            }
            // get new events
            let result = self.make_request_blocking_q("/rest/events", &[("since", self.last_event_id.to_string())]);
            match result {
                Ok(value) => {
                    if let Some(events) = value.as_array() {
                        // get last events for 'FolderCompletion'
                        // -> so this will not be handled multiple times
                        // here also consider events that happened before startup
                        let last_folder_completion_events = self.syncthing_folders.iter().map(|folder| {
                           events.iter().rev().find(|event| {
                               //println!("test ----  {}", event["type"].as_str().unwrap_or(""));
                               let mut found = event["type"].as_str().unwrap_or("") == "FolderCompletion";
                               if let Some(data) = event.get("data") {
                                   found &= data["folder"].as_str().unwrap_or("") == folder.0;
                               }
                               else {
                                   found = false;
                               }
                               found
                           })
                        });



                        // handle events (without FolderCompletion)
                        for event in events {
                            self.last_event_id = event["id"].as_i64().unwrap_or(-1);
                            let event_type = event["type"].as_str().unwrap_or("??");
                            let event_time = DateTime::parse_from_rfc3339(event["time"].as_str().unwrap_or("??"));
                            if event_type == "Unknown" || event_type == "FolderCompletion" {
                                continue;
                            }
                            if self.config_yaml.syncthing.ignore_events_before_startup {
                                match event_time {
                                    Ok(time) => {
                                        //println!("###### time {}", time);
                                        if time <= self.startup_time {
                                            continue;
                                        }
                                    }
                                    Err(e) => {
                                        println!("could not read event time '{}'", e);
                                    }
                                }
                            }

                            //println!("-- got syncthing event (id: {}): {}", self.last_event_id, event_type);
                            if let Some(data) = event.get("data") {
                                if let Err(e) = self.handle_event(event_type, data) {
                                    println!("could not handle event: {}", e);
                                    println!("-- event json: {}", data.to_string());
                                }
                            }
                        }


                        // handle 'FolderCompletion' events
                        for last_folder_completion_event in last_folder_completion_events {
                            if let Some(event) = last_folder_completion_event {
                                println!("-- got syncthing event FolderCompletion (id: {})", self.last_event_id);
                                if let Some(data) = event.get("data") {
                                    if let Err(e) = self.handle_folder_completion_event(data) {
                                        println!("could not handle event: {}", e);
                                        println!("-- event json: {}", data.to_string());
                                    }
                                }
                            }
                            //println!("#### {:?}", last_folder_completion_event);
                        }
                    }
                }
                Err(err) => {
                    println!("request to syncthing events failed: {}", err);
                    println!("-- will try again");
                    std::thread::sleep(Duration::from_secs(5));
                }
            };
            //return Ok(())
        }
    }

    fn pull_folders_from_config(&mut self) -> Result<()> {
        let config = self.make_request_blocking(self.syncthing_config_sub_url.as_str())?;
        let folders = config["folders"].as_array().context("response json wrong")?;
        let mut seen_folders: HashSet<String> = HashSet::new();
        for folder in folders.iter() {
            let id = folder["id"].as_str().context("response json wrong").unwrap();
            let path = folder["path"].as_str().context("response json wrong").unwrap().to_string();
            match self.syncthing_folders.get_mut(id) {
                None => {
                    self.syncthing_folders.insert(id.to_string(), Folder{
                        path
                    });
                }
                Some(f) => {
                    f.path = path;
                }
            }
            seen_folders.insert(id.to_string());
        }
        self.syncthing_folders.retain(|el, _| seen_folders.contains(el));
        if self.verbose {
            println!("updated syncthing_folders: {:?}", self.syncthing_folders.iter().map(|el| format!("{} - {:?}", el.0, el.1)).collect::<Vec<String>>());
        }
        Ok(())
    }

    /*
    fn get_folder_path(&self, folder_id: &str) -> Result<String> {
        let config = self.make_request_blocking(self.syncthing_config_sub_url.as_str())?;
        let path = config["folders"].as_array().context("response json wrong")?.iter()
            .find(|el| el["id"].as_str().context("response json wrong").unwrap() == folder_id)
            //.ok_or(Err(""))?
            .context(format!("folder with id '{}' not found in syncthing config", folder_id))?
            .get("path").context("response json wrong")?
            .as_str().context("response json wrong")?.to_string();
        return Ok(path);
    }
    */


    fn handle_folder_completion_event(&self, event_data: &Value) -> Result<()> {
        let folder_id = event_data["folder"].as_str().context("response json format wrong")?;
        let folder = Path::new(self.syncthing_folders.get(folder_id).context("folder not found")?.path.as_str()).to_owned();
        let completion = event_data["completion"].as_i64().context("response json format wrong")?;
        println!("--> folder (id: {}) '{}' sync completed by {}%", folder_id, folder.to_str().context("")?, completion);

        if completion >= 100 {
            (self.on_dir_synced)(folder.as_path());
        }
        Ok(())
    }

    fn handle_event(&self, event_type: &str, event_data: &Value) -> Result<()> {
        match event_type {
            "LocalChangeDetected" => {
                let folder = Path::new(event_data["folder"].as_str().context("response json format wrong")?);
                (self.on_dir_synced)(folder);
            }

            "StateChanged" => {
                let folder_id = event_data["folder"].as_str().context("response json format wrong")?;
                let folder = Path::new(self.syncthing_folders.get(folder_id).context("folder not found")?.path.as_str()).to_owned();
                let to_state = event_data["to"].as_str().context("response json format wrong")?;
                println!("--> folder (id: {}) '{}' changed to state '{}'", folder_id, folder.to_str().context("")? , to_state);
                // (self.on_dir_synced)(folder.as_path());
            }
            _ => {}
        }

        Ok(())
    }
}